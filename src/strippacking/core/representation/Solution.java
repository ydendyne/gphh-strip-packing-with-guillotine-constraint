package strippacking.core.representation;

import strippacking.core.Bin;
import strippacking.core.Instance;
import strippacking.core.Item;
import strippacking.core.Vector;

import java.util.LinkedList;
import java.util.List;

public class Solution<B extends Bin> {

    List<B> bins;
    List<Item> items;
    int currentItem;
    double stripWidth;

    public Solution(List<Item> items, double stripWidth) {
        this.bins = new LinkedList<>();
        this.items = new LinkedList<>(items);
        this.stripWidth = stripWidth;
        this.currentItem = 0;
    }

    public static Solution<Bin> initialBasicStripPackingSolution(Instance instance){
        // first bin
        // TODO incorporate a mechanism to allow shelf packing or level packing
//        instance.getItems().get(0).
        //make bin of size of first package
        //update bins

//        Item item = s.nextItem();
//
//        // create a bin dimension vector
//        int dos = item.getDimensionVector().getDimension();
//        double width = instance.getStripWidth();
//        double[] vec = new double[item.getDimensionVector().getDimension()];
//        vec[0] = width;
//        for (int i = 1; i < vec.length; i++) {
//            vec[i] = item.getDimensionVector().getDimensions()[i];
//        }
//        Vector v = new Vector(dos, vec);
//        Bin b = new Bin(v,new Vector(dos, new double[dos]), 0);
//        b.placeItem(item);
//        s.add(b);

        return new Solution<>(instance.getItems(), instance.getStripWidth());
    }

    public boolean add(B b){
        return bins.add(b);
    }

    public void setItems(List<Item> items){
        this.items = items;
    }

    public Item nextItem(){
        if (currentItem == items.size()) {
            return null;
        }else {
            return items.get(currentItem++);
        }
    }


    public List<B> getBins() {
        return new LinkedList<>(bins);
    }

    public double getStripWidth() {
        return stripWidth;
    }

    public boolean isComplete(){
        return currentItem == items.size();
    }
}
