package strippacking.core;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InstanceSamples {
    Instance baseInstance;
    List<Long> seeds;

    public InstanceSamples(Instance baseInstance, List<Long> seeds) {
        this.baseInstance = baseInstance;
        this.seeds = seeds;
    }

    public InstanceSamples(Instance baseInstance) {
        this(baseInstance, new ArrayList<>());
    }

    public List<Long> getSeeds() {
        return new LinkedList<>(seeds);
    }

    public long getSeed(int i){
        return seeds.get(i);
    }

    public Instance getBaseInstance() {
        return baseInstance;
    }

    public void setSeed(int i, long seed){
        seeds.set(i, seed);
    }

    public void addSeed(long seed){
        seeds.add(seed);
    }
}
