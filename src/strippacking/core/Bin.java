package strippacking.core;

import java.util.ArrayList;

public class Bin {

    /**
     * The order of bins created
     */
    private int id;

    /**
     * The dimensions of the bin (can be in Nd space)
     */
    private Vector dimensionVector;

    /**
     * Position of the bin and if it has been placed or not
     */
    private Vector positionVector;
    private boolean isPlaced;

    /**
     * Items in the bin and next available position (to the right of the last item)
     */
    private ArrayList<Item> items;
    private Vector nextPosition;


    public Bin(Vector dimensions, Vector position, int id) {
        this.dimensionVector = dimensions;
        this.positionVector = position;
        this.id = id;
        this.isPlaced = false;
        this.items = new ArrayList<>();
        // zero vector (relative to the bin)
        this.nextPosition = new Vector(dimensions.getDimension(), new double[dimensions.getDimension()]);
    }

    /**
     * Place an item into the bin WITH ASSUMPTION THAT PLACEMENT IS FEASIBLE!
     *
     * positionVector is relative to the bin ( 0,0 being the bottom left corner of the bin)
     *                       the bin lives in the non negative orthant
     */
    public void placeItem(Item item){
        item.linkBin(Vector.clone(nextPosition), this);
        this.items.add(item);

        //move the next position vector over in respect to the first dimension (eg width or x)
        double[] pv = nextPosition.getDimensions();
        pv[0] += item.getDimensionVector().getDimensions()[0];
        nextPosition = new Vector(nextPosition.getDimension(), pv);
    }

    /**
     * Place an item into the bin WITH ASSUMPTION THAT PLACEMENT IS FEASIBLE!
     *
     * positionVector is relative to the bin ( 0,0 being the bottom left corner of the bin)
     *                       the bin lives in the non negative orthant
     */
    public ArrayList<Bin> cutItem(Item item){

        ArrayList<Bin> bins = new ArrayList<>();
        item.linkBin(Vector.clone(nextPosition), this);
        this.items.add(item);

        double[] pv = new double[positionVector.getDimension()];
        for (int i = 0; i < pv.length; i++) {
            pv[i] = positionVector.getDimensions()[i];
        }
        pv[0] += item.getDimensionVector().getDimensions()[0];
        Vector rightPosition = new Vector(positionVector.getDimension(), pv);

        double[] pvt = new double[positionVector.getDimension()];
        for (int i = 0; i < pvt.length; i++) {
            pvt[i] = positionVector.getDimensions()[i];
        }
        pvt[pvt.length - 1] += item.getDimensionVector().getDimensions()[pvt.length - 1];
        Vector topPosition = new Vector(positionVector.getDimension(), pvt);

        double[] dimR = new double[this.dimensionVector.getDimension()];
        for (int i = 0; i < dimR.length; i++) {
            dimR[i] = this.getDimensionVector().getDimensions()[i];
        }
        dimR[0] -= item.getDimensionVector().getDimensions()[0];
        Vector dimensionRight = new Vector(this.dimensionVector.getDimension(), dimR);

        double[] dimT = new double[this.dimensionVector.getDimension()];
        for (int i = 0; i < dimT.length; i++) {
            dimT[i] = this.getDimensionVector().getDimensions()[i];
        }
        dimT[dimT.length - 1] = this.getDimensionVector().getDimensions()[dimT.length - 1] - item.getDimensionVector().getDimensions()[dimT.length - 1];
        dimT[0] = item.getDimensionVector().getDimensions()[0];
        Vector dimensionTop = new Vector(this.dimensionVector.getDimension(), dimT);

        Bin rightBin = new Bin(dimensionRight, rightPosition, this.id + 1);
        Bin topBin = new Bin(dimensionTop, topPosition, this.id + 2);

        // make the position vector infeasible
        double[] pvv = nextPosition.getDimensions();
        pvv[0] += 100000;
        nextPosition = new Vector(nextPosition.getDimension(), pvv);

        bins.add(rightBin);
        bins.add(topBin);
        return bins;

//        Bin bin = new Bin()

        //move the next position vector over in respect to the first dimension (eg width or x)

    }

    /**
     * Returns true if a specified item fits into the bin at the current position (within the bin)
     *
     * @param item specified item
     * @return if item fits
     */
    public boolean isFeasible(Item item){
        for (int i = 0; i < dimensionVector.getDimension(); i++) {
            if (nextPosition.getDimensions()[i] + item.getDimensionVector().getDimensions()[i] > dimensionVector.getDimensions()[i]){
                // if the item is intersecting with the bin
                return false;
            }
        }
        return true;
    }

    /**
     * gets the hyper volume to the right of an item (if placed)
     *
     * @param item specified item
     * @return area/volume/hyper-volume of the area to the right of the item if placed into the bin
     */
    public double areaToRight(Item item){
        // get the x value to the right of the package
        double area = dimensionVector.getDimensions()[0] - (nextPosition.getDimensions()[0] + item.getDimensionVector().getDimensions()[0]);
        for (int i = 1; i < dimensionVector.getDimension(); i++) {
            // multiply by other dimensions of the bin
            area *= dimensionVector.getDimensions()[i];
        }

        return area;
    }

    /**
     * gets the hyper volume above an item (if placed)
     *
     * @param item specified item
     * @return area/volume/hyper-volume of the area above the item if placed into the bin
     */
    public double areaAbove(Item item){
        // get the x value to the right of the package
        double area = item.getDimensionVector().getDimensions()[0];
        for (int i = 1; i < dimensionVector.getDimension(); i++) {
            // multiply by other dimensions of the bin
            area *= dimensionVector.getDimensions()[i] - item.getDimensionVector().getDimensions()[i];
        }

        return area;
    }

    /**
     * returns the difference between the area of the bin and the cumulative area of the items plus the
     * specified item
     *
     * @param item specified item
     * @return the total wastage after a specified item is added
     */
    public double totalWastage(Item item){
        double bin_hv = Vector.getArea(dimensionVector);
        double items_hv = 0;
        for (Item item1 : this.items) {
            items_hv += Vector.getArea(item1.getDimensionVector());
        }
        items_hv += Vector.getArea(item.getDimensionVector());
        return bin_hv - items_hv;
    }

    /**
     * ================================================================
     *                             GETTERS
     * ================================================================
     */

    public Vector getDimensionVector() {
        return dimensionVector;
    }

    public Vector getPositionVector() {
        return positionVector;
    }

    public boolean isPlaced() {
        return isPlaced;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Item> getItems() {
        return new ArrayList<>(items);
    }

    public boolean hasItems(){
        return !items.isEmpty();
    }

    /**
     * ================================================================
     *                             Setters
     * ================================================================
     */

    public void setPositionVector(Vector positionVector) {
        this.positionVector = positionVector;
        this.isPlaced = true;
    }

}
