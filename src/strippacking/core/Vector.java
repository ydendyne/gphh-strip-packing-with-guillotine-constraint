package strippacking.core;

public class Vector {

    /**
     * R^(dimension) the dimension of space
     */
    private final int dimension;

    /**
     * a n dimensional vector
     */
    private final double[] dimensions;

    public Vector(int dimension, double[] dimensions) {
        this.dimension = dimension;
        this.dimensions = dimensions;
    }

    public int getDimension() {
        return dimension;
    }

    public double[] getDimensions() {
        return dimensions;
    }

    public static double getArea(Vector v){
        double total = 1;
        for (double dimension : v.getDimensions()) {
            total *= dimension;
        }

        return total;
    }

    public static Vector clone(Vector v){
        double[] vec = new double[v.getDimension()];
        for (int i = 0; i < vec.length; i++) {
            vec[i] = v.getDimensions()[i];
        }

        return new Vector(v.getDimension(), vec);
    }

    public static Vector plus(Vector a, Vector b){
        if (a.getDimension() != b.getDimension())return null;

        double[] vec = new double[a.getDimension()];

        for (int i = 0; i < vec.length; i++) {
            vec[i] = a.getDimensions()[i] + b.getDimensions()[i];
        }

        return new Vector(a.getDimension(), vec);
    }

}
