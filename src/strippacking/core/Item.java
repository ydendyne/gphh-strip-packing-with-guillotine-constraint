package strippacking.core;

public class Item {

    /**
     * The dimensions of the bin (can be in Nd space)
     */
    private final Vector dimensionVector;

    /**
     * position vector is relative to the bin that it is placed into
     */
    private Vector positionVector;
    private boolean isPlaced;
    private Bin bin;

    /**
     * Id of the package (determined by the instance file)
     */
    private final int id;

    public Item(Vector dimensions, int id) {
        this.dimensionVector = dimensions;
        this.id = id;
    }

    /**
     * ================================================================
     *                             GETTERS
     * ================================================================
     */

    public Vector getDimensionVector() {
        return dimensionVector;
    }

    public int getId() {
        return id;
    }

    public Vector getPositionVector() {
        return positionVector;
    }

    public boolean isPlaced() {
        return isPlaced;
    }

    public Bin getBin() {
        return bin;
    }

    /**
     * ================================================================
     *                             SETTERS
     * ================================================================
     */

    public void linkBin(Vector positionVector, Bin bin){
        this.bin = bin;
        this.positionVector = positionVector;
        this.isPlaced = true;
    }


}
