package strippacking.core;

import org.apache.commons.lang3.math.NumberUtils;
import org.jfree.util.StringUtils;

import java.io.*;
import java.util.*;

public class Instance {
    private String name = null; // the name of the instance

    /**
     * Determines the width of the strip to cut items out of
     */
    private double stripWidth;

    /**
     * The collection of items that need to be cut from the strip
     */
    private List<Item> items;

    /**
     * set to the zero vector to give an origin of a solution
     * and the dimensionality of space
     */
    private Vector dimensionOfSpace;

    /**
     * Global seed for this instance
     */
    private long seed;

    public Instance(String name, double stripWidth, List<Item> items, Vector dimensionOfSpace, long seed) {
        this.name = name;
        this.stripWidth = stripWidth;
        this.items = items;
        this.dimensionOfSpace = dimensionOfSpace;
        this.seed = seed;
    }

    /**
     * ================================================================
     *                             GETTERS
     * ================================================================
     */

    public List<Item> getItems() {
        return new LinkedList<>(items);
    }

    public long getSeed() {
        return seed;
    }

    public double getStripWidth() {
        return stripWidth;
    }

    public String getName() {
        return name;
    }

    public Vector getDimensionOfSpace() {
        return dimensionOfSpace;
    }

    public int getNumItems(){
        return items.size();
    }

    /**
     * ================================================================
     *                             SETTERS
     * ================================================================
     */

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * ================================================================
     *                             PARSERS
     * ================================================================
     */

    public static Instance readFromFile(File file, long seed){

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            String instanceName = "";
            double sw = -1;
            ArrayList<Item> itms = new ArrayList<>();
            Vector dos = null;

            String s;

            while ((s = br.readLine()) != null) {

                String[] str = s.split(" ");

                if (str.length > 0){
                    if (str[0].equalsIgnoreCase("N")){
                        instanceName = str[1];
                    }else if (str[0].equalsIgnoreCase("D")){ // dimension
                        dos = new Vector(Integer.parseInt(str[1]), new double[Integer.parseInt(str[1])]);
                    }else if (str[0].equalsIgnoreCase("W")){ // strip width
                        sw = Double.parseDouble(str[1]);
                    }else if (NumberUtils.isParsable(str[0])){ // item
                        int id = Integer.parseInt(str[0]);
                        double[] dim = new double[str.length - 1];
                        for (int i = 1; i < str.length; i++) {
                            dim[i - 1] = Double.parseDouble(str[i]);
                        }
                        Vector dimVector = new Vector(str.length-1, dim);
                        itms.add(new Item(dimVector, id));
                    }
                }
            }

            Collections.shuffle(itms, new Random(seed));

            return new Instance(instanceName, sw, itms, dos, seed);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    /**
     * ================================================================
     *                             Cloning
     * ================================================================
     */

    public static Instance clone(Instance i, long seed){
        List<Item> itms = new ArrayList<>(i.items);
        Collections.shuffle(itms, new Random(seed));
        return new Instance(i.name, i.stripWidth, itms, i.dimensionOfSpace, seed);
    }

}
