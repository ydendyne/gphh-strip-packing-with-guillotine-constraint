package strippacking.decisionprocess.poolfilters;


import strippacking.core.Bin;
import strippacking.core.Item;
import strippacking.decisionprocess.DecisionProcessState;
import strippacking.decisionprocess.PoolFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * This filter selects only the tasks that are expected to be feasible.
 * It also filters out the tasks if the way from the current node to them
 * passes the depot. The motivation is that this can be replaced by
 * refill-then-serve.
 */

public class BasicStripPackingPoolFilter extends PoolFilter {

    @Override
    public List<Bin> filter(List<Bin> pool, Item item, DecisionProcessState state) {
        // returns a list of feasible bins (remove infeasible bins from list)
        ArrayList<Bin> filtered = new ArrayList<>();

        for (Bin bin : pool) {
            if (bin.isFeasible(item)){
                filtered.add(bin);
            }
        }

        return filtered;
    }
}
