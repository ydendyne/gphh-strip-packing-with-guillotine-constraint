package strippacking.decisionprocess.tiebreakers;

import strippacking.core.Bin;
import strippacking.core.Vector;
import strippacking.decisionprocess.TieBreaker;


public class BasicBinTieBreaker extends TieBreaker {
    @Override
    public int breakTie(Bin bin1, Bin bin2) {
        // choose the smallest bin
        if (Vector.getArea(bin1.getDimensionVector()) > Vector.getArea(bin2.getDimensionVector())){
            return 1;
        }else if (Vector.getArea(bin1.getDimensionVector()) < Vector.getArea(bin2.getDimensionVector())){
            return -1;
        }else{
            // if they're equal just go with the first
            return 1;
        }
    }
}
