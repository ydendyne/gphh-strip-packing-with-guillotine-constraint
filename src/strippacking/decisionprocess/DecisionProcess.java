package strippacking.decisionprocess;


import strippacking.core.Bin;
import strippacking.core.Instance;
import strippacking.core.representation.Solution;
import strippacking.decisionprocess.basicmethod.BasicDecisionProcess;
import strippacking.decisionprocess.basicmethod.BasicPackingEvent;
import strippacking.decisionprocess.basicmethod.PackingEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * An abstract of a decision process. A decision process is a process where
 * vehicles make decisions as they go to serve the tasks of the graph.
 * It includes
 *  - A decision process state: the state of the vehicles and the environment
 *  - An event queue: the events to happen
 *  - A routing policy that makes decisions as the vehicles go.
 *  - A task sequence solution as a predefined plan. This is used for proactive-reactive decision process.
 */

public abstract class DecisionProcess {
    protected DecisionProcessState state; // the state
    protected List<PackingEvent> eventQueue;
    protected PlacementPolicy placementPolicy;
    protected Solution<Bin> plan;

    public DecisionProcess(DecisionProcessState state,
                           List<PackingEvent> eventQueue,
                           PlacementPolicy placementPolicy,
                           Solution<Bin> plan) {
        this.state = state;
        this.eventQueue = eventQueue;
        this.placementPolicy = placementPolicy;
        this.plan = plan;
    }

    public DecisionProcessState getState() {
        return state;
    }

    public List<PackingEvent> getEventQueue() {
        return eventQueue;
    }

    /**
     * Run the decision process.
     */
    static int run = 0;
    public void run() {
        // first sample the random variables by the seed.
        state.getInstance().setSeed(state.getSeed());

//        System.out.println("----------------------- New run -----------------------");

//        double start = System.currentTimeMillis();

        // trigger the events.
        //System.out.println("QUEUE SIZE = " + eventQueue.size());

        while (!eventQueue.isEmpty()) {
            PackingEvent event = eventQueue.get(0);
            //System.out.println("asfdasfsadfd");
            event.trigger(this);
            eventQueue.remove(event);
        }

//        double end = System.currentTimeMillis();
//        double time = end - start;
//        System.out.println("time: " + time + " routing policy: " + routingPolicy.getClass());
    }


    /**
     * Reset the decision process.
     * This is done by reseting the decision process state and event queue.
     */
//    public abstract void reset();

    public PlacementPolicy getPlacementPolicy() {
        return placementPolicy;
    }

    public void setPlan(Solution<Bin> plan){
        this.plan = plan;
    }

    public static BasicDecisionProcess initBasic(Instance i, long seed, PlacementPolicy pp ){
        DecisionProcessState state = new DecisionProcessState(i, seed);
        List<PackingEvent> pq = new ArrayList<>();

        return new BasicDecisionProcess(state, pq, pp, state.getSolution());
    }

    public abstract DecisionProcess getInstance(DecisionProcessState state_clone, List<PackingEvent> eventQueue_clone,
                                                PlacementPolicy policy_clone, Solution<Bin> plan_clone);

}
