package strippacking.decisionprocess;

import strippacking.core.Bin;
import strippacking.core.Instance;
import strippacking.core.representation.Solution;
import strippacking.core.Item;

import java.util.*;

/**
 * The state for the decision making process of a reactive solution builder.
 * It includes
 *  - the given UCARP instance, including relevant information such as the graph,
 *  - a list of remaining unserved tasks,
 *  - a list of unassigned tasks,
 *  - a partial node sequence solution.
 * Created by gphhucarp on 27/08/17.
 */
public class DecisionProcessState {
    private Instance instance; // the UCARP instance
    private long seed; // the seed to sample the random variables in the UCARP instance
    private List<Item> remainingItems;
    private Solution<Bin> solution;

    // global information here (global wastage etc)

    public DecisionProcessState(Instance instance, long seed, List<Item> remainingItems, Solution<Bin> solution) {
        this.instance = instance;
        this.seed = seed;
        this.remainingItems = remainingItems;
        this.solution = solution;
    }


    public DecisionProcessState(Instance instance, long seed) {
        this.instance = instance;
        this.seed = seed;
        this.remainingItems = new LinkedList<>(instance.getItems());
//        Collections.shuffle(this.remainingItems, new Random(seed));
        solution = Solution.initialBasicStripPackingSolution(instance);
    }

    public List<Bin> getBins(){
        return solution.getBins();
    }


//    public DecisionProcessState(Instance instance, long seed) {
//        this(instance, seed, instance.getNumVehicles());
//    }

    // ---------- GETTERS / SETTERS -------------

    public Instance getInstance() {
        return instance;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

//    public void reset(Dec initialState) {
//        remainingTasks.clear();
//        for (Arc task : initialState.remainingTasks) {
//        }
//    }

    /**
     * Reset a decision process state as the initial state.
     */
//    public void reset() {
//        remainingTasks = new LinkedList<>(instance.getTasks());
//        taskRemainingDemandFrac.clear();
//        for (Arc task : remainingTasks)
//            taskRemainingDemandFrac.put(task, 1.0);
//        unassignedTasks = new LinkedList<>(remainingTasks);
//        solution.reset(instance);
//
//        resetTaskToTaskMap();
//        resetRouteToTaskMap();
//
//    }


//    public NodeSeqRoute getRoute(List<Boolean> ret, NodeSeqRoute match){
//        for(int i = 0; i < solution.getRoutes().size(); i++) {
//            if (!ret.get(i) && match.equals(solution.getRoute(i))) {
//                ret.set(i, true);
//                return solution.getRoute(i);
//            }
//        }
//
//        System.out.println("Error: there is no matching route: NodeSeqRoute. There is a major error in your cloning process.");
//        new Exception().printStackTrace();
//        System.exit(0);
//        return null;
//    }


    public Solution<Bin> getSolution() {
        return solution;
    }
}
