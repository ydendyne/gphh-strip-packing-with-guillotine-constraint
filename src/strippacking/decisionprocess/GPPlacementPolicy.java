package strippacking.decisionprocess;

import ec.EvolutionState;
import ec.gp.GPTree;
import ec.util.Parameter;
import strippacking.decisionprocess.poolfilters.BasicStripPackingPoolFilter;

public class GPPlacementPolicy extends GPPlacementPolicy_frame {

    public static boolean recordData;

    public GPPlacementPolicy(){ super(); }

    public GPPlacementPolicy(PoolFilter poolFilter, GPTree[] gpTrees) {
        super(poolFilter, gpTrees);
    }

    public GPPlacementPolicy(PoolFilter poolFilter, GPTree gpTree){
        super(poolFilter, new GPTree[]{gpTree});
    }

    public GPPlacementPolicy(GPTree[] gpTrees) {
        this(new BasicStripPackingPoolFilter(), gpTrees);
    }

    @Override
    public void setup(EvolutionState state, Parameter base) {
        Parameter p = base.push(P_RECORDDATA);
        recordData = state.parameters.getBoolean(p, null, false);
    }

    @Override
    public GPPlacementPolicy_frame newTree(PoolFilter poolFilter, GPTree[] gpTree) {
        return new GPPlacementPolicy(poolFilter, gpTree);
    }

    @Override
    public boolean recordingData() {
        return false;
    }


}
