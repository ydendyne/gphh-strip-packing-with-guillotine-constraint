package strippacking.decisionprocess.basicmethod;

import strippacking.core.Bin;
import strippacking.core.Item;
import strippacking.decisionprocess.DecisionProcess;

public abstract class PackingEvent{
    public abstract void trigger(DecisionProcess decisionProcess);

}
