package strippacking.decisionprocess.basicmethod;

import strippacking.core.Bin;
import strippacking.core.Instance;
import strippacking.core.Item;
import strippacking.core.Vector;
import strippacking.core.representation.Solution;
import strippacking.decisionprocess.DecisionProcess;
import strippacking.decisionprocess.DecisionProcessState;
import strippacking.decisionprocess.PlacementPolicy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class BasicDecisionProcess extends DecisionProcess {

    public BasicDecisionProcess(DecisionProcessState state, List<PackingEvent> eventQueue, PlacementPolicy placementPolicy, Solution<Bin> plan) {
        super(state, eventQueue, placementPolicy, plan);
    }

    public static BasicDecisionProcess initBP(Instance instance, long seed, PlacementPolicy placementPolicy){
        DecisionProcessState state = new DecisionProcessState(instance, seed);
        List<PackingEvent> pq = new ArrayList<>();

        Item initItem = state.getSolution().nextItem();
        // create a bin dimension vector
        int dos = initItem.getDimensionVector().getDimension();
        double width = state.getSolution().getStripWidth();
        double[] vec = new double[initItem.getDimensionVector().getDimension()];
        vec[0] = width;
        for (int i = 1; i < vec.length; i++) {
            vec[i] = initItem.getDimensionVector().getDimensions()[i];
        }
        Vector v = new Vector(dos, vec);
        Bin b = new Bin(v,new Vector(dos, new double[dos]), 0);

        state.getSolution().add(b);

        BasicPackingEvent bpe = new BasicPackingEvent(initItem, b);

        pq.add(bpe);

        return new BasicDecisionProcess(state, pq, placementPolicy, state.getSolution());
    }

//    @Override
//    public void reset() {
//        state.
//
//    }

    @Override
    public DecisionProcess getInstance(DecisionProcessState state_clone, List<PackingEvent> eventQueue_clone, PlacementPolicy policy_clone, Solution<Bin> plan_clone) {
        BasicDecisionProcess bp = new BasicDecisionProcess(state_clone, eventQueue_clone, policy_clone, plan_clone);
        bp.setPlan(plan_clone);
        return bp;
    }
}
