package strippacking.decisionprocess.basicmethod;

import strippacking.core.Bin;
import strippacking.core.Item;
import strippacking.decisionprocess.DecisionProcessState;
import strippacking.decisionprocess.DecisionSituation;

import java.util.List;

public class BasicDecisionSituation extends DecisionSituation {

    List<Bin> pool;
    Item item;
    DecisionProcessState state;

    public BasicDecisionSituation(List<Bin> pool, Item item, DecisionProcessState state) {
        this.pool = pool;
        this.item = item;
        this.state = state;
    }

    public List<Bin> getPool() {
        return pool;
    }

    public Item getItem() {
        return item;
    }

    public DecisionProcessState getState() {
        return state;
    }

}
