package strippacking.decisionprocess.basicmethod;

import strippacking.core.Bin;
import strippacking.core.Item;
import strippacking.core.Vector;
import strippacking.decisionprocess.DecisionProcess;
import strippacking.decisionprocess.DecisionProcessState;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * The reactive refill event occurs when the vehicle is going back to the depot.
 * The target node is the depot, and there is no next task.
 * The vehicle does not have the next task to serve,
 * as no remaining task is feasible before the refill.
 * When it arrives the depot and finish the refill, it will start serving again.
 */

public class BasicPackingEvent extends PackingEvent {

    Item item;
    Bin bin;

    public BasicPackingEvent(Item item, Bin bin) {
        super();
        this.item = item;
        this.bin = bin;
    }

    @Override
    public void trigger(DecisionProcess decisionProcess) {
        // set the current event's process - add it to the solution
            // TODO implement
            // this object has an item and bin, edit your solution to place item in bin
//        bin.placeItem(item);

        // decide on the next event and add it to the event queue
            // decisionProcess.getEventQueue().add(some event)
        // set the item's next bin

        //System.out.println("here?");

        ArrayList<Bin> bins = this.bin.cutItem(this.item);

        DecisionProcessState state = decisionProcess.getState();

        for (Bin bin1 : bins) {
            state.getSolution().add(bin1);
        }

        List<Bin> pool = new LinkedList<>(state.getBins());

        Item nextItem = decisionProcess.getState().getSolution().nextItem();

        if (nextItem == null){
            return;
        }

        BasicDecisionSituation bdsm = new BasicDecisionSituation(pool, nextItem, state);

        // magic line
        Bin nextBin = decisionProcess.getPlacementPolicy().next(bdsm, decisionProcess);

//        System.out.println("next bin: " + ((nextBin!=null)?nextBin.toString():"its null"));
        // needs to be cut from the strip
        if (nextBin == null){

            int dos = nextItem.getDimensionVector().getDimension();
            double[] vec = new double[dos];
            vec[0] = state.getInstance().getStripWidth();
//                    bin.getDimensionVector().getDimensions()[0];
            for (int i = 1; i < vec.length; i++) {
                vec[i] = nextItem.getDimensionVector().getDimensions()[i];
            }

            double[] posVec = new double[dos];

            Bin maxBin = state.getSolution().getBins().get(0);
            for (Bin bin : state.getSolution().getBins()) {
                if (bin.getPositionVector().getDimensions()[dos - 1] > maxBin.getPositionVector().getDimensions()[dos - 1]){
                    maxBin = bin;
                }
            }

            for (int i = 0; i < dos; i++) {
                posVec[i] = maxBin.getPositionVector().getDimensions()[i] + maxBin.getDimensionVector().getDimensions()[i];
            }
            posVec[0] = 0;
            posVec[dos - 1] = maxBin.getPositionVector().getDimensions()[dos - 1] + maxBin.getDimensionVector().getDimensions()[dos - 1];

            nextBin = new Bin(new Vector(dos, vec), new Vector(dos, posVec), maxBin.getId() + 1);

            state.getSolution().add(nextBin);
        }

//        System.out.println("next bin: " + nextBin.toString());
//        nextBin.placeItem(nextItem);

        BasicPackingEvent bpe = new BasicPackingEvent(nextItem, nextBin);

        decisionProcess.getEventQueue().add(bpe);

    }


    /*

    OLD METHOD

     */
//    @Override
//    public void trigger(DecisionProcess decisionProcess) {
//        // set the current event's process - add it to the solution
//        // this object has an item and bin, edit your solution to place item in bin
////        bin.placeItem(item);
//
//        // decide on the next event and add it to the event queue
//        // decisionProcess.getEventQueue().add(some event)
//        // set the item's next bin
//
//        //System.out.println("here?");
//
//        this.bin.placeItem(this.item);
//
//        DecisionProcessState state = decisionProcess.getState();
//
//        List<Bin> pool = new LinkedList<>(state.getBins());
//
//        Item nextItem = decisionProcess.getState().getSolution().nextItem();
//
//        if (nextItem == null){
//            return;
//        }
//
//        BasicDecisionSituation bdsm = new BasicDecisionSituation(pool, nextItem, state);
//
//        // magic line
//        Bin nextBin = decisionProcess.getPlacementPolicy().next(bdsm, decisionProcess);
//
////        System.out.println("next bin: " + ((nextBin!=null)?nextBin.toString():"its null"));
//        // needs to be cut from the strip
//        if (nextBin == null){
//
//            int dos = nextItem.getDimensionVector().getDimension();
//            double[] vec = new double[dos];
//            vec[0] = state.getInstance().getStripWidth();
////                    bin.getDimensionVector().getDimensions()[0];
//            for (int i = 1; i < vec.length; i++) {
//                vec[i] = nextItem.getDimensionVector().getDimensions()[i];
//            }
//
//            double[] posVec = new double[dos];
//
//            Bin maxBin = state.getSolution().getBins().get(0);
//            for (Bin bin : state.getSolution().getBins()) {
//                if (bin.getPositionVector().getDimensions()[dos - 1] > maxBin.getPositionVector().getDimensions()[dos - 1]){
//                    maxBin = bin;
//                }
//            }
//
//            for (int i = 0; i < dos; i++) {
//                posVec[i] = maxBin.getPositionVector().getDimensions()[i] + maxBin.getDimensionVector().getDimensions()[i];
//            }
//            posVec[0] = 0;
//            posVec[dos - 1] = maxBin.getPositionVector().getDimensions()[dos - 1] + maxBin.getDimensionVector().getDimensions()[dos - 1];
//
//            nextBin = new Bin(new Vector(dos, vec), new Vector(dos, posVec), maxBin.getId() + 1);
//
//            state.getSolution().add(nextBin);
//        }
//
////        System.out.println("next bin: " + nextBin.toString());
////        nextBin.placeItem(nextItem);
//
//        BasicPackingEvent bpe = new BasicPackingEvent(nextItem, nextBin);
//
//        decisionProcess.getEventQueue().add(bpe);
//
//    }


}
