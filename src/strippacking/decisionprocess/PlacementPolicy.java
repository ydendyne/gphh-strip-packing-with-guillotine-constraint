package strippacking.decisionprocess;

import ec.EvolutionState;
import ec.gp.GPTree;

import ec.util.Parameter;
import gputils.DoubleData;
import strippacking.core.Bin;
import strippacking.core.Item;
import strippacking.decisionprocess.basicmethod.BasicDecisionSituation;
import strippacking.decisionprocess.poolfilters.BasicStripPackingPoolFilter;
import strippacking.decisionprocess.tiebreakers.BasicBinTieBreaker;
import strippacking.gp.CalcPriorityProblem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A packing policy makes a decision
 */

public abstract class PlacementPolicy {

    protected String name;
    protected PoolFilter poolFilter;
    protected TieBreaker tieBreaker;


    public PlacementPolicy(PoolFilter poolFilter, TieBreaker tieBreaker) {
        this.poolFilter = poolFilter;
        this.tieBreaker = tieBreaker;
    }

    public PlacementPolicy(PoolFilter poolFilter) {
        this(poolFilter, new BasicBinTieBreaker());
    }

    public PlacementPolicy(TieBreaker tieBreaker) {
        this(new BasicStripPackingPoolFilter(), tieBreaker);
    }

    public PlacementPolicy() {}

    public String getName() {
        return name;
    }

    public PoolFilter getPoolFilter() {
        return poolFilter;
    }

    public TieBreaker getTieBreaker() {
        return tieBreaker;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Given the current decison process state,
     * select the next task to serve by the give route from the pool of tasks.
     * @param rds the reactive decision situation.
     * @return the next task to be served by the route.
     */
    protected HashMap<Bin, Double> priorities;
    protected List<Bin> candidates;
    public Bin next(BasicDecisionSituation bds, DecisionProcess dp) {
        List<Bin> pool = bds.getPool();
        Item item = bds.getItem();
        DecisionProcessState state = bds.getState();

        List<Bin> filteredPool = poolFilter.filter(pool, item, state);

        if (filteredPool.isEmpty()) {
            return null;
        }

        priorities = new HashMap<>();
        candidates = new ArrayList<>();

        Bin next = filteredPool.get(0);

        priorities.put(next, priority(next, item, state));

        for (int i = 1; i < filteredPool.size(); i++) {
            Bin tmp = filteredPool.get(i);
            priorities.put(tmp, priority(tmp, item, state));

            if (Double.compare(priorities.get(tmp), priorities.get(next)) < 0 ||
                    (Double.compare(priorities.get(tmp), priorities.get(next)) == 0 &&
                            tieBreaker.breakTie(tmp, next) < 0))
                next = tmp;
        }

        return next;
    }


    private GPTree[] gpTrees;

    public GPTree[] getGPTrees() {
        return gpTrees;
    }

    public double priority(Bin bin, Item item, DecisionProcessState state) {
        CalcPriorityProblem calcPrioProb =
                new CalcPriorityProblem(bin, item, state);

        DoubleData tmp = new DoubleData();
        gpTrees[0].child.eval(null, 0, tmp, null, null, calcPrioProb);

        return tmp.value;
    }

    public abstract PlacementPolicy newTree(PoolFilter pf, GPTree[] gpt);

    public abstract void setup(final EvolutionState state, final Parameter base);
}
