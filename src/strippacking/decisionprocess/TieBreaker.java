package strippacking.decisionprocess;

import strippacking.core.Bin;

import java.util.List;

/**
 * A tie breaker breaks the tie between two arcs when they have the same priority.
 */

public abstract class TieBreaker {

    public abstract int breakTie(Bin bin1, Bin bin2);
}
