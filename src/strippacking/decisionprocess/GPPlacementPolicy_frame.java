package strippacking.decisionprocess;

import ec.EvolutionState;
import ec.gp.GPTree;
import ec.util.Parameter;
import gputils.DoubleData;

import java.util.List;

public abstract class GPPlacementPolicy_frame extends PlacementPolicy{

    public static final String P_RECORDDATA = "record-data";
    public double numDecisions = 0.0;
    public double sumDecisionTime = 0.0;

    public static GPPlacementPolicy_frame secondaryPolicy;

    private GPTree[] gpTrees;

    public GPPlacementPolicy_frame(PoolFilter poolFilter, GPTree[] gpTrees) {
        super(poolFilter);
        name = "\"GPPlacementPolicy\"";
        this.gpTrees = gpTrees;
    }

    @Override
    public Object clone() {
        return null;
    }

    public abstract void setup(final EvolutionState state, final Parameter base);

    public GPPlacementPolicy_frame() {}

    public abstract GPPlacementPolicy_frame newTree(PoolFilter poolFilter, GPTree[] gpTree);

    public abstract boolean recordingData();

    public GPTree[] getGPTrees() {
        return gpTrees;
    }

    public void setGPTree(GPTree[] gpTrees) {
        this.gpTrees = gpTrees;
    }

    public void setSecondaryPolicy(GPPlacementPolicy_frame sec){
        secondaryPolicy = sec;
    }


}
