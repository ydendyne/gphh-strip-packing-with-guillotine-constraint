package strippacking.decisionprocess.placementpolicy;

import ec.EvolutionState;
import ec.gp.GPTree;
import ec.util.Parameter;
import gputils.DoubleData;
import strippacking.core.Bin;
import strippacking.core.Item;
import strippacking.decisionprocess.DecisionProcessState;
import strippacking.decisionprocess.PlacementPolicy;
import strippacking.decisionprocess.PoolFilter;
import strippacking.gp.CalcPriorityProblem;

public class BasicPlacementPolicy extends PlacementPolicy {
    public GPTree[] gpTrees;

    public BasicPlacementPolicy(PoolFilter poolFilter, GPTree[] gpTrees) {
        super(poolFilter);
        this.gpTrees = gpTrees;
    }

    public BasicPlacementPolicy() {
        super();
    }

    @Override
    public double priority(Bin bin, Item item, DecisionProcessState state) {
        CalcPriorityProblem calcPrioProb =
                new CalcPriorityProblem(bin, item, state);

        DoubleData tmp = new DoubleData();
        gpTrees[0].child.eval(null, 0, tmp, null, null, calcPrioProb);

        return tmp.value;
    }

    @Override
    public PlacementPolicy newTree(PoolFilter pf, GPTree[] gpt) {
        return new BasicPlacementPolicy(pf, gpt);
    }

    @Override
    public void setup(EvolutionState state, Parameter base) {

    }

    public GPTree[] getGpTrees() {
        return gpTrees;
    }
}
