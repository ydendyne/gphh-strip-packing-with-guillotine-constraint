package strippacking.gp;

import ec.EvolutionState;
import ec.Individual;
import ec.Problem;
import ec.simple.SimpleProblemForm;

import strippacking.core.Bin;
import strippacking.core.Item;
import strippacking.decisionprocess.DecisionProcessState;

import java.util.List;

/**
 * The problem for calculating the priority of a candidate task.
 *
 * Created by YiMei on 27/09/16.
 */
public class CalcPriorityProblem extends Problem implements SimpleProblemForm {

    private Bin candidate;
    private Item item;
    private DecisionProcessState state;

    public CalcPriorityProblem(Bin candidate,
                               Item item,
                               DecisionProcessState state) {
        this.candidate = candidate;
        this.item = item;
        this.state = state;
    }

    public Bin getCandidate() {
        return candidate;
    }

    public Item getItem() {
        return item;
    }

    public DecisionProcessState getState() {
        return state;
    }

    @Override
    public void evaluate(EvolutionState state, Individual ind, int subpopulation, int threadnum) {

    }
}
