package strippacking.gp.terminal.feature;

import strippacking.gp.CalcPriorityProblem;
import strippacking.gp.terminal.FeatureGPNode;

public class BinWidth extends FeatureGPNode {
    public BinWidth(){
        super();
        name = "BW";
    }

    @Override
    public double value(CalcPriorityProblem calcPriorityProblem) {
        return calcPriorityProblem.getCandidate().getDimensionVector().getDimensions()[1];
    }

}
