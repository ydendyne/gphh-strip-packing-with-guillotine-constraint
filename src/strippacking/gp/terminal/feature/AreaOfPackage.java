package strippacking.gp.terminal.feature;

import strippacking.core.Vector;
import strippacking.gp.CalcPriorityProblem;
import strippacking.gp.terminal.FeatureGPNode;

public class AreaOfPackage extends FeatureGPNode {
    public AreaOfPackage(){
        super();
        name = "AOP";
    }

    @Override
    public double value(CalcPriorityProblem calcPriorityProblem) {
        return Vector.getArea(calcPriorityProblem.getItem().getDimensionVector());
    }

}
