package strippacking.gp.terminal.feature;

import strippacking.gp.CalcPriorityProblem;
import strippacking.gp.terminal.FeatureGPNode;

public class BinWastedAreaToRight extends FeatureGPNode {
    public BinWastedAreaToRight(){
        super();
        name = "BWR";
    }

    @Override
    public double value(CalcPriorityProblem calcPriorityProblem) {
        return calcPriorityProblem.getCandidate().areaToRight(calcPriorityProblem.getItem());
    }

}
