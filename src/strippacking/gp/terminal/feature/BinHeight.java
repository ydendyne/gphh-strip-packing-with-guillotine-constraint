package strippacking.gp.terminal.feature;

import strippacking.gp.CalcPriorityProblem;
import strippacking.gp.terminal.FeatureGPNode;

public class BinHeight extends FeatureGPNode {
    public BinHeight(){
        super();
        name = "BH";
    }

    @Override
    public double value(CalcPriorityProblem calcPriorityProblem) {
        return calcPriorityProblem.getCandidate().getDimensionVector().getDimensions()[1];
    }

}
