package strippacking.gp.terminal.feature;

import strippacking.gp.CalcPriorityProblem;
import strippacking.gp.terminal.FeatureGPNode;

public class BinWastedAreaAbove extends FeatureGPNode {
    public BinWastedAreaAbove(){
        super();
        name = "BWA";
    }

    @Override
    public double value(CalcPriorityProblem calcPriorityProblem) {
        return calcPriorityProblem.getCandidate().areaAbove(calcPriorityProblem.getItem());
    }

}
