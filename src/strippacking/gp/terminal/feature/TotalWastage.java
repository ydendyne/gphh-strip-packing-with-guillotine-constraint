package strippacking.gp.terminal.feature;

import strippacking.gp.CalcPriorityProblem;
import strippacking.gp.terminal.FeatureGPNode;

public class TotalWastage extends FeatureGPNode {
    public TotalWastage(){
        super();
        name = "TW";
    }

    @Override
    public double value(CalcPriorityProblem calcPriorityProblem) {
        return calcPriorityProblem.getCandidate().totalWastage(calcPriorityProblem.getItem());
    }

}
