package strippacking.gp;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.gp.GPTree;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;
import gputils.LispUtils;
import strippacking.decisionprocess.PlacementPolicy;
import strippacking.decisionprocess.PoolFilter;
import strippacking.decisionprocess.TieBreaker;
import strippacking.decisionprocess.placementpolicy.BasicPlacementPolicy;
import strippacking.gp.evaluation.EvaluationModel;

import java.util.List;

public class SPPGProblem extends GPProblem implements SimpleProblemForm {

    public static final String P_EVAL_MODEL = "eval-model";
    public static final String P_POOL_FILTER = "pool-filter";
    public static final String P_TIE_BREAKER = "tie-breaker";
    public static final String P_PLACEMENT_POLICY = "placement-policy";

    public EvaluationModel evaluationModel;
    protected PoolFilter poolFilter;
    protected TieBreaker tieBreaker;
    protected PlacementPolicy prototypePolicy;

    public void rotateEvaluationModel() {
        evaluationModel.rotateSeeds();
    }


    @Override
    public void setup(final EvolutionState state, final Parameter base) {
        super.setup(state, base);

        Parameter p = base.push(P_EVAL_MODEL);
        evaluationModel = (EvaluationModel)(
                state.parameters.getInstanceForParameter(
                        p, null, EvaluationModel.class));
        evaluationModel.setup(state, p);

        p = base.push(P_POOL_FILTER);
        poolFilter = (PoolFilter)(
                state.parameters.getInstanceForParameter(
                        p, null, PoolFilter.class));

        p = base.push(P_TIE_BREAKER);
        tieBreaker = (TieBreaker)(
                state.parameters.getInstanceForParameter(
                        p, null, TieBreaker.class));

        p = base.push(P_PLACEMENT_POLICY);
        if(state.parameters.exists(p)) {
            prototypePolicy = (PlacementPolicy) (
                    state.parameters.getInstanceForParameter(
                            p, null, PlacementPolicy.class));

            prototypePolicy.setup(state, p);
        }

    }

    public PlacementPolicy buildPlacementPolicy(List<String> expressions) {
        int numTrees = expressions.size();
        GPTree[] trees = new GPTree[numTrees];
        for (int i = 0; i < numTrees; i++) {
            trees[i] = LispUtils.parseExpression(expressions.get(i), SPPGPrimitiveSet.wholePrimitiveSet());
        }
//        prototypePolicy.newTree(poolFilter, new GPTree[]{trees[0]});
        return prototypePolicy.newTree(poolFilter, new GPTree[]{trees[0]});
    }

    @Override
    public void evaluate(EvolutionState state, Individual ind, int subpopulation, int threadnum) {
        PlacementPolicy pp = null;
        if (prototypePolicy != null){
            pp = prototypePolicy.newTree(poolFilter, ((GPIndividual) ind).trees);
        }
        evaluationModel.evaluate(pp, null, ind.fitness, state);
    }
}
