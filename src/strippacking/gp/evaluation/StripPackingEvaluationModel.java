package strippacking.gp.evaluation;

import ec.EvolutionState;
import ec.Fitness;
import ec.multiobjective.MultiObjectiveFitness;
import strippacking.core.*;
import strippacking.core.representation.Solution;
import strippacking.decisionprocess.DecisionProcess;
import strippacking.decisionprocess.PlacementPolicy;
import strippacking.decisionprocess.basicmethod.BasicDecisionProcess;
import strippacking.decisionprocess.basicmethod.BasicPackingEvent;

import java.util.Arrays;
import java.util.List;

public class StripPackingEvaluationModel extends EvaluationModel{

    public void printSolution(Solution<Bin> solution){

        List<Bin> bins = solution.getBins();

        System.out.println("-------------------------------------------------------");
        System.out.println();

        for (Bin bin : bins) {
            System.out.println("Bin: " + bin.getId());
            System.out.println("    D: " + Arrays.toString(bin.getDimensionVector().getDimensions()));
            System.out.println("    P: " + Arrays.toString(bin.getPositionVector().getDimensions()));
            System.out.println("    Items{ ");
            for (Item item : bin.getItems()) {
                System.out.println("        I: " + item.getId() + " D: " + Arrays.toString(item.getDimensionVector().getDimensions())
                        + " P: " + Arrays.toString(item.getPositionVector().getDimensions()));
            }
        }

        System.out.println();
        System.out.println("-------------------------------------------------------");
        System.out.println();


    }

    public void animationOutput(Solution<Bin> solution){
        System.out.println(solution.getStripWidth());
        for (Bin bin : solution.getBins()) {
            for (Item item : bin.getItems()) {
                System.out.print(item.getId() + " ");
                for (double dimension : item.getDimensionVector().getDimensions()) {
                    System.out.print(dimension + " ");
                }
                for (double dimension : Vector.plus(bin.getPositionVector(), item.getPositionVector()).getDimensions()) {
                    System.out.print(dimension + " ");
                }
                System.out.print("\n");
            }
        }
        System.out.println();

    }

    /**
     * Training
     *
     * @param policy the policy to be evaluated.
     * @param plan the plan to be evaluated -- null if the policy is reactive.
     * @param fitness the fitness of the individual.
     * @param state the evolution state.
     */
    @Override
    public void evaluate(PlacementPolicy policy, Solution<Bin> plan, Fitness fitness, EvolutionState state) {
        double[] fitnesss = new double[1];

        int decisionProcesses = 0;

        for (InstanceSamples instanceSample : instanceSamples) {

            for (Long seed : instanceSample.getSeeds()) {
                Instance sampleInstance = Instance.clone(instanceSample.getBaseInstance(), seed);
//                BasicDecisionProcess bp = DecisionProcess.initBasic(instanceSample.getBaseInstance(), seed, policy);
                BasicDecisionProcess bp = BasicDecisionProcess.initBP(sampleInstance, seed, policy);

                bp.run();

                Solution<Bin> solution = bp.getState().getSolution();

//                if (solution.isComplete()){
////                    printSolution(solution);
//                    animationOutput(solution);
//                }

                Bin maxBin = solution.getBins().get(0);
                int dimensionOfSpace = maxBin.getDimensionVector().getDimension();
                for (Bin bin : solution.getBins()) {
                    if (bin.hasItems() &&
                            bin
                                    .getPositionVector()
                                    .getDimensions()[dimensionOfSpace -1] >
                                    maxBin.getPositionVector().getDimensions()[dimensionOfSpace -1]){
                        maxBin = bin;
                    }
//                    System.out.println("FUCK " + solution.getBins().size() + ", items :" + bin.getItems().size());
                }

                Item maxItem = maxBin
                        .getItems()
                        .get(0);

                for (Item item : maxBin.getItems()) {
                    if (item.getDimensionVector().getDimensions()[dimensionOfSpace - 1] > maxItem.getDimensionVector().getDimensions()[dimensionOfSpace - 1]){
                        maxItem = item;
                    }
                }

                fitnesss[0] += maxBin.getPositionVector().getDimensions()[dimensionOfSpace - 1]
                        + maxItem.getPositionVector().getDimensions()[dimensionOfSpace - 1]
                        + maxItem.getDimensionVector().getDimensions()[dimensionOfSpace - 1];

                decisionProcesses -=- 1;
            }

        }

        fitnesss[0] /= decisionProcesses;

        MultiObjectiveFitness mof = (MultiObjectiveFitness) fitness;
        mof.setObjectives(state, fitnesss);

    }

    /**
     * Test
     *
     * @param policy the policy to be evaluated.
     * @param plan the plan to be evaluated -- null if the policy is reactive.
     * @param fitness the fitness of the individual.
     * @param state the evolution state.
     */
    @Override
    public void evaluateOriginal(PlacementPolicy policy, Solution<Bin> plan, Fitness fitness, EvolutionState state) {
        evaluate(policy, plan, fitness, state);
    }

}
