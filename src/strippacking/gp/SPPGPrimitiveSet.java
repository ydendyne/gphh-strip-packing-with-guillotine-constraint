package strippacking.gp;

import gputils.terminal.TerminalERCUniform;
import strippacking.gp.terminal.feature.*;
import gputils.function.*;
import gputils.terminal.PrimitiveSet;

public class SPPGPrimitiveSet extends PrimitiveSet {


    public static SPPGPrimitiveSet basicTerminalSet(){
        SPPGPrimitiveSet sp = new SPPGPrimitiveSet();

        // Problem Specific
        sp.add(new BinWastedAreaAbove());
        sp.add(new BinWastedAreaToRight());
        sp.add(new AreaOfPackage());
        sp.add(new BinWidth());
        sp.add(new BinHeight());
        sp.add(new TotalWastage());

        // problem independent
//        sp.add(new TerminalERCUniform());

        return sp;
    }

    public static SPPGPrimitiveSet wholePrimitiveSet(){
        SPPGPrimitiveSet sp = basicTerminalSet();

        // functions (non-terminals)
        sp.add(new Add());
        sp.add(new Sub());
        sp.add(new Mul());
        sp.add(new Div());
        sp.add(new Max());
        sp.add(new Min());
        sp.add(new If());

        return sp;
    }

}

